<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nueva tienda</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="bs-callout bs-callout-warning">
					<h4><span class="glyphicon glyphicon-edit"></span> Crear nueva tienda</h4>
					<p>Ingresa el nombre de la nueva Tienda</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="well">
					<form:form method="post" id="formTienda" modelAttribute="tienda" class="form-horizontal">
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Nombre: </label>
							<div class="col-md-8">
								<form:input path="nombre" class="form-control" type="text" id="nombre" placeholder="Nombre tienda" />
								<form:errors path="nombre" cssClass="error"></form:errors>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-6">
								<input type="submit" class="btn btn-warning" value="Registrar">
								<a class="btn btn-danger" href="${pageContext.request.contextPath}/index">Cancelar</a>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>