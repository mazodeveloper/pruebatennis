<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro Exitoso</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="bs-callout bs-callout-info">
					<h2><span class="glyphicon glyphicon-ok"></span> ${mensaje}</h2>
					<a class="btn btn-info" href="${pageContext.request.contextPath}/index">Volver al Inicio.</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>