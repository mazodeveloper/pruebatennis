<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tienda</title>
</head>
<body>
	<div class="wrapper">
		<div class="sidebar-wrapper">
			<h4 class="containerStore"><a id="newStore" href="${pageContext.request.contextPath}/${tienda.nombre}/producto/0">Productos 
				<span class="glyphicon glyphicon-plus"></span></a>
			</h4>
			<ul class="lista-nav">
				<c:forEach items="${tienda.listaProductos}" var="producto" varStatus="contador"> 
					<li>
						<a class="store" href="${pageContext.request.contextPath}/${tienda.nombre}/producto/${producto.id}">${producto.nombre}</a>
					</li>
				</c:forEach>
			</ul>
		</div>
		
		<div class="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-xs-12">
						<div class="bs-callout bs-callout-info">
							<h2><span class="glyphicon glyphicon-shopping-cart"></span> ${tienda.nombre}</h2>
							<p>Preciona el boton para ver el listado de productos pertenecientes al ${tienda.nombre}.</p>
							<a href="#" id="menu-toggle" class="btn btn-default"><span class="glyphicon glyphicon-align-justify"></span></a>
							<a href="${pageContext.request.contextPath}/index" class="btn btn-danger">
								<span class="glyphicon glyphicon-chevron-left"></span> Regresar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
					
	
<script type="text/javascript">
	$(document).ready(efecto);
	
	function efecto(){
		$("#menu-toggle").click(function(e){
			e.preventDefault();
			$(".wrapper").toggleClass("menuDisplayed");
		});
	}
	
</script>
</body>
</html>