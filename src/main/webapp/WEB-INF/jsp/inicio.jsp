<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inicio</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/slideBar.css">
</head>
<body>
	<div class="wrapper">
		<div class="sidebar-wrapper">
			<h4 class="containerStore"><a id="newStore" href="${pageContext.request.contextPath}/tienda/0">Tiendas 
				<span class="glyphicon glyphicon-plus"></span></a>
			</h4>
			<ul class="lista-nav">
				<c:forEach items="${listaTiendas}" var="tienda" varStatus="contador"> 
					<li>
						<a class="store" href="${pageContext.request.contextPath}/tienda/${tienda.id}">${tienda.nombre}</a>
						<button class="btn btn-danger btnDelete" value="${tienda.id}">
								<span class="glyphicon glyphicon-remove"></span>
						</button>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div class="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<div class="bs-callout bs-callout-info">
							<h2><span class="glyphicon glyphicon-home"></span> ${mensaje}</h2>
							<p>Preciona el boton para ver el listado de nuestras tiendas.</p>
							<a href="#" id="menu-toggle" class="btn btn-default"><span class="glyphicon glyphicon-align-justify"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

<script type="text/javascript">
	$(document).ready(deleteTienda);
	
	function deleteTienda(){
		$(".btnDelete").click(function(){
			var boton = $(this);
			var valor = $(this).val();
			$.ajax({
				type : 'DELETE',
				headers : {
					accept : 'application/json; charset=utf-8'
				},
				url : '${pageContext.request.contextPath}/tienda/'+valor
			});
			boton.parents("li").remove();
		});	
	}	
</script>
<script type="text/javascript">
	$("#menu-toggle").click(function(e){
		e.preventDefault();
		$(".wrapper").toggleClass("menuDisplayed");
	});
</script>	
</body>
</html>