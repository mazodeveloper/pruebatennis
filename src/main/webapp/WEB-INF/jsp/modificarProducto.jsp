<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Producto</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="bs-callout bs-callout-warning">
					<h4><span class="glyphicon glyphicon-edit"></span> Actualizar producto.</h4>
					<p>llena los siguientes campos para actualizar el producto.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="well">
					<form:form method="post" modelAttribute="productoResource" action="${pageContext.request.contextPath}/producto/${productoId}/updateCompleto" 
					class="form-horizontal" id="formProducto" enctype="multipart/form-data">
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Nombre: </label>
							<div class="col-md-8">
								<form:input path="nombre" class="form-control" type="text" id="nombre" placeholder="Nombre producto" />
								<form:errors path="nombre" cssClass="error"></form:errors>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Descripcion: </label>
							<div class="col-md-8">
								<form:textarea path="descripcion" class="form-control" rows="6" cols="80" id="descripcion" placeholder="Escribe una peque�a descripcion del producto"/>
								<form:errors path="descripcion" cssClass="error"></form:errors>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Precio: </label>
							<div class="col-md-8">
								<form:input path="precio" class="form-control" type="text" id="precio" placeholder="Precio del producto"/>
								<form:errors path="precio" cssClass="error"></form:errors>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Imagen: </label>
							<div class="col-md-8">
								<form:input path="imagen" type="file" id="imagen" />
								<form:errors path="imagen" cssClass="error"></form:errors>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-sm-hidden control-label">Tienda: </label>
							<div class="col-md-8">
								<form:select path="tienda" id="tienda" class="form-control">
									<c:forEach items="${listaTiendas}" var="tienda" varStatus="contador">
										<c:choose>
											<c:when test="${tienda.id == productoResource.tienda}">
												<form:option selected="selected" value="${tienda.id}">${tienda.nombre}</form:option>
											</c:when>
											<c:otherwise>
												<form:option value="${tienda.id}">${tienda.nombre}</form:option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</form:select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-2">
								<input type="submit" value="Registrar" class="btn btn-warning">
								<a class="btn btn-danger" href="${pageContext.request.contextPath}/tienda/${productoResource.tienda}">Cancelar</a>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>