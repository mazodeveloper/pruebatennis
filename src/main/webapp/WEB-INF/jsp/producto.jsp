<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Producto</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/detallesProducto.css">
</head>
<body>
	
	<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="bs-callout bs-callout-info">
						<h4><span class="glyphicon glyphicon-shopping-cart"></span> Producto de la tienda ${producto.tienda.nombre}</h4>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div  class="col-md-5 col-xs-12">
					<a class="thumbnail">
						<img id="imagenGrande" src="${pageContext.request.contextPath}/resources/image/${producto.imagen}">
					</a>
			    </div>
				<div  class="col-md-5 col-md-offset-1 col-xs-12">
					<h2>${producto.nombre}</h2>
					<h3 class="precio">$ ${producto.precio}</h3>
					<a class="btn btn-info" href="${pageContext.request.contextPath}/${producto.tienda.nombre}/producto/${producto.id}/modificar">
						<span class="glyphicon glyphicon-pencil"></span> Modificar
					</a>				
					<a class="btn btn-danger" href="${pageContext.request.contextPath}/${producto.tienda.nombre}/producto/${producto.id}/delete">
						<span class="glyphicon glyphicon-remove"></span> Eliminar
					</a>				
					<a class="btn btn-warning" href="${pageContext.request.contextPath}/tienda/${tiendaId}">
						<span class="glyphicon glyphicon-arrow-left"></span> volver tienda
					</a>
				</div>
			</div>
			
			<div id="boxDescription" class="row">
				<div class="col-md-12 col-xs-12 caja">
					<h3>${producto.nombre}</h3><br>
					<p class="descripcion">${producto.descripcion}, este gran producto lo puedes encontrar en la tienda 
						<span style="text-transform: uppercase;">${producto.tienda.nombre}</span>
					</p>
				</div>
			</div>
				
	</div>		
				
	
</body>
</html>