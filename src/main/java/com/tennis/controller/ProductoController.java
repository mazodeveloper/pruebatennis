package com.tennis.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.tennis.entities.Producto;
import com.tennis.entities.Tienda;
import com.tennis.repository.ProductoRepository;
import com.tennis.repository.TiendaRepository;
import com.tennis.resources.ProductoResource;

@Controller
public class ProductoController {

	@Autowired
	private ProductoRepository productoJpa;
	@Autowired
	private TiendaRepository tiendaJpa;

	/* Este metodo evalua el Id del producto, si este es 0 se retorna un formulario para registrar un nuevo producto, si es diferente a 0,
	 * se retorna el producto identificado con dicho id */
	@RequestMapping(value="/{nombreTienda}/producto/{productoId}", method=RequestMethod.GET)
	public String getProducto(@PathVariable(value="productoId")Integer productoId, @PathVariable(value="nombreTienda")String nombreTienda,
			Model model){
		
		ProductoResource productoResource;
		Tienda tienda = tiendaJpa.findTiendaPorNombre(nombreTienda);
		Integer tiendaId = tienda.getId();
		Iterable<Tienda> listaTiendas = tiendaJpa.findAll();
		
		model.addAttribute("tiendaId", tiendaId);
		
		if(productoId == 0){
			
			productoResource = new ProductoResource();
			model.addAttribute("productoResource", productoResource);
			model.addAttribute("listaTiendas", listaTiendas);
			model.addAttribute("nombreTienda", nombreTienda);
			
			return "nuevoProducto";
		}
		
		Producto producto = productoJpa.findOne(productoId);
		model.addAttribute("producto", producto);
		
		return "producto";
	}
	
	/* Este metodo es el encargado de registrar un producto nuevo, si existe algun error en los datos enviados por el usuario, se retornara la 
	 * pagina del formulario indicandole al usuario los errores que debe corregir. */
	@RequestMapping(value="/{nombreTienda}/registroFinalizado", method=RequestMethod.POST)
	public String createProducto(@ModelAttribute @Valid ProductoResource productoResource, BindingResult result, HttpServletRequest request,
			@PathVariable(value="nombreTienda")String nombreTienda, Model model) throws IllegalStateException, IOException{
		
		if(result.hasErrors()){
			Iterable<Tienda> listaTiendas = tiendaJpa.findAll();
			Tienda tienda = tiendaJpa.findTiendaPorNombre(nombreTienda);
			Integer tiendaId = tienda.getId();
			
			model.addAttribute("listaTiendas", listaTiendas);
			model.addAttribute("tiendaId", tiendaId);
			
			return "nuevoProducto";
		}
		
		String directorio= request.getServletContext().getRealPath("/resources/image/");
		
		Tienda tienda = tiendaJpa.findOne(productoResource.getTienda());
		
		Producto producto = new Producto();
		producto.setNombre(productoResource.getNombre());
		producto.setDescripcion(productoResource.getDescripcion());
		producto.setPrecio(productoResource.getPrecio());
		producto.setTienda(tienda);
		
		MultipartFile imagen = productoResource.getImagen();
		String nombreImagen = imagen.getOriginalFilename();
		
		if(!"".equalsIgnoreCase(nombreImagen)){
			
			imagen.transferTo(new File(directorio + nombreImagen));	
		}
		
		producto.setImagen(nombreImagen);
		
		productoJpa.save(producto);
		
		return "redirect:/productoSuccess";
	}
	
	@RequestMapping(value="productoSuccess")
	public String registroTerminado(Model model){
		
		model.addAttribute("mensaje", "Felicidades has registrado exitosamente un nuevo producto.");
		return "registroExitoso";
	}
	
	/* Este metodo retornara un formulario el cual nos permitira modificar los datos que deseemos del producto */
	@RequestMapping(value="/{nombreTienda}/producto/{productoId}/modificar", method=RequestMethod.GET)
	public String formUpdateProducto(@PathVariable(value="productoId")Integer productoId, @PathVariable(value="nombreTienda")String nombreTienda, 
			Model model){
		
		Producto producto = productoJpa.findOne(productoId);
		Iterable<Tienda> listaTiendas = tiendaJpa.findAll();
		ProductoResource productoResource = new ProductoResource();
		
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setPrecio(producto.getPrecio());
		productoResource.setTienda(producto.getTienda().getId());
		
		model.addAttribute("productoResource", productoResource);
		model.addAttribute("listaTiendas", listaTiendas);
		model.addAttribute("productoId", productoId);
		
		return "modificarProducto";
	} 
	
	/*En este metodo realizamos el update del producto, tambien se manejan validaciones con el fin de administrar los errores ingresados por el
	 *  usuario */
	@RequestMapping(value="/producto/{productoId}/updateCompleto", method=RequestMethod.POST)
	public String updateProducto(@Valid @ModelAttribute ProductoResource productoResource, BindingResult result, 
			@PathVariable(value="productoId")Integer productoId, HttpServletRequest request, Model model) throws IllegalStateException, IOException {
		
		if(result.hasErrors()){
			Iterable<Tienda> listaTiendas = tiendaJpa.findAll();
					
			model.addAttribute("productoResource", productoResource);
			model.addAttribute("listaTiendas", listaTiendas);
			model.addAttribute("productoId", productoId);
			
			return "modificarProducto";
		}
		
		String directorio= request.getServletContext().getRealPath("/resources/image/");
		
		Producto producto = productoJpa.findOne(productoId);
		producto.setNombre(productoResource.getNombre());
		producto.setDescripcion(productoResource.getDescripcion());
		producto.setPrecio(productoResource.getPrecio());
		producto.setTienda(tiendaJpa.findOne(productoResource.getTienda()));
		
		MultipartFile imagen = productoResource.getImagen();
		String nombreImagen = imagen.getOriginalFilename();
		
		if(!"".equalsIgnoreCase(nombreImagen)){
			
			/*Dado que este metodo es update evaluamos la nueva imagen ingresada por el usuario y la antigua imagen del producto, si son diferentes 
			 * eliminamos la antigua imagen del servidor y creamos la nueva imagen ingresada por el usuario. */
			if(!nombreImagen.equals(producto.getImagen())){
				File file = new File(directorio + producto.getImagen());
				file.delete();
				imagen.transferTo(new File(directorio + nombreImagen));
				
				producto.setImagen(nombreImagen);
			}			
		}		
		
		productoJpa.save(producto);
				
		return "redirect:/updateFinished";
	}
	
	@RequestMapping(value="updateFinished")
	public String updateFinished(Model model){
		
		model.addAttribute("mensaje", "Felicidades has actualizado exitosamente tu producto.");
		return "registroExitoso";
	}
	
	/* este metodo realiza la tarea de eliminar un producto y redireccionar a la vista de la tienda dueña del producto. */
	@RequestMapping(value="/{nombreTienda}/producto/{productoId}/delete", method=RequestMethod.GET)
	public String deleteProducto(@PathVariable(value="productoId")Integer productoId, @PathVariable(value="nombreTienda")String nombreTienda){
		
		Tienda tienda = tiendaJpa.findTiendaPorNombre(nombreTienda);
		productoJpa.delete(productoId);
		
		return "redirect:/tienda/"+tienda.getId();
	}
}
