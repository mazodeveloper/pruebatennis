package com.tennis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tennis.entities.Tienda;
import com.tennis.repository.TiendaRepository;

@Controller
public class TiendaController {

	@Autowired
	private TiendaRepository tiendaJpa;
	
	@RequestMapping(value="/index")
	public String getTiendas(Model model){
		
		Iterable<Tienda> listaTiendas = tiendaJpa.findAll();
		
		model.addAttribute("mensaje", "Listado de tiendas Tennis");
		model.addAttribute("listaTiendas", listaTiendas);
				
		return "inicio";
	}
	
	@RequestMapping(value="/tienda/{tiendaId}", method=RequestMethod.DELETE, produces="application/json")
	public void deleteTienda(@PathVariable(value="tiendaId")Integer tiendaId){
		
		tiendaJpa.delete(tiendaId);		
	}
	
	
	@RequestMapping(value="/tienda/{tiendaId}", method=RequestMethod.GET)
	public String getTienda(@PathVariable(value="tiendaId")Integer tiendaId, Model model){
		
		Tienda tienda;
		
		if(tiendaId == 0){
			
			tienda = new Tienda();
			model.addAttribute("tienda", tienda);
			
			return "nuevaTienda";
			
		}else{
			tienda = tiendaJpa.findOne(tiendaId);
			model.addAttribute("tienda", tienda);
			
			return "tienda";
		}
	}

	@RequestMapping(value="/tienda/0", method=RequestMethod.POST)
	public String createTienda(@Valid @ModelAttribute Tienda tienda, BindingResult result){
		
		if(result.hasErrors()){
			return "nuevaTienda";
		}
		tiendaJpa.save(tienda);
		
		return "redirect:/index";
	}
}
