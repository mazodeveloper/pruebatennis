package com.tennis.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageValidation{

	private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
	
	private Pattern pattern;
	private Matcher matcher;
	
	public ImageValidation() {
		
		pattern = Pattern.compile(IMAGE_PATTERN);
	}
	
	public Boolean validar(final String nombreImagen){
		
		matcher = pattern.matcher(nombreImagen);
		return matcher.matches();		
	}
}
