package com.tennis.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="tienda")
public class Tienda implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="nombre")
	@NotNull(message="El campo nombre es requerido.")
	@NotEmpty(message="El campo nombre es requerido.")
	@Size(min= 3, max=14, message="el campo debe ser mayor a 3 y menor a 14 caracteres.")
	private String nombre;
	
	@OneToMany(mappedBy="tienda", fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
	private Set<Producto> listaProductos;

	
	public Integer getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(Set<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	
}
