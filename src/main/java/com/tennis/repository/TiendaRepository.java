package com.tennis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tennis.entities.Tienda;

@Repository
public interface TiendaRepository extends CrudRepository<Tienda, Integer>{
	
	@Query(value="SELECT t.* FROM tienda t WHERE t.nombre = ?1", nativeQuery=true)
	public Tienda findTiendaPorNombre(String nombre);
	
}
