package com.tennis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tennis.entities.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Integer>{
	
	
}
