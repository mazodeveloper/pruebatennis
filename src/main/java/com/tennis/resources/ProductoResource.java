package com.tennis.resources;

import java.math.BigDecimal;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class ProductoResource {
	
	
	@NotEmpty(message="El campo nombre es requerido.")
	private String nombre;
	
	
	@NotEmpty(message="El campo descripcion es requerido.")
	private String descripcion;
	
	private BigDecimal precio;
	
	private MultipartFile imagen;
	
	private Integer tienda;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	public MultipartFile getImagen() {
		return imagen;
	}
	public void setImagen(MultipartFile imagen) {
		this.imagen = imagen;
	}
	public Integer getTienda() {
		return tienda;
	}
	public void setTienda(Integer tienda) {
		this.tienda = tienda;
	}
		
}
